from gi.repository import Gtk

from .constants import KEYS_TO_SESSIONS


class SessionsButtonsBox(Gtk.HBox):
    buttons = []

    def __init__(self, switch_session_func=None, **kwargs):
        super().__init__(spacing=2, **kwargs)

        for name, i in KEYS_TO_SESSIONS.items():  # 1, 2, .., 9, 0
            group = None if i == 0 else self.buttons[0]
            button = Gtk.RadioButton.new_from_widget(group)
            button.set_label(name)
            button.set_focus_on_click(False)
            button.set_mode(False)
            button.connect("toggled", switch_session_func, i)
            self.pack_start(button, False, False, 0)

            self.buttons.append(button)


class HeaderBar(Gtk.HeaderBar):
    session_buttons_box = None

    def __init__(self, multiple_sessions=False, switch_session_func=None, **kwargs):
        super().__init__(**kwargs)

        # Show 'close' button (also 'minimize' and 'maximize', if they are visible).
        self.set_show_close_button(True)

        if multiple_sessions:
            self.session_buttons_box = SessionsButtonsBox(switch_session_func=switch_session_func)
            self.add(self.session_buttons_box)

    def activate_session_button(self, button_idx):
        button = self.session_buttons_box.buttons[button_idx]
        button.set_active(not button.get_active())


