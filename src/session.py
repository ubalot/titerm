"""
Session is implemented as a binary tree. Its nodes are either Paned or Terminal
widget.
"""

from gi.repository import Gtk

from .constants import HORIZONTAL_ORIENTATION, VERTICAL_ORIENTATION
from .terminal import Terminal


class SessionNode:
    def __init__(self, session, orientation):
        self.session = session
        self.orientation = orientation
        self.left_node = None
        self.right_node = None

    @property
    def session(self):
        return self.__session

    @session.setter
    def session(self, session):
        if not isinstance(session, Session):
            raise ValueError("SessionNode::session -> SessionNode must be child of some Session, actual value: {} .".format(session))

        self.__session = session

    @property  # getter
    def orientation(self):
        return self._orientation

    @orientation.setter  # setter
    def orientation(self, orientation):
        if orientation != HORIZONTAL_ORIENTATION and orientation != VERTICAL_ORIENTATION:
            raise ValueError("SessionNode::orientation -> wrong value '{}' : this is nor horizontal, nor vertical.".format(orientation))

        self._orientation = orientation

    @property  # getter
    def left_node(self):
        return self._left_node

    @left_node.setter  # setter
    def left_node(self, widget):
        if isinstance(widget, Terminal) or isinstance(widget, SessionNode):
            self._left_node = widget
        else:
            self._left_node = None
            if widget is not None:
                raise ValueError("SessionNode::left_node -> wrong value '{}' : this must be a Terminal or a SessionNode or None.".format(widget))

    @property  # getter
    def right_node(self):
        return self._right_node

    @right_node.setter  # setter
    def right_node(self, widget):
        if isinstance(widget, Terminal) or isinstance(widget, SessionNode):
            self._right_node = widget
        else:
            self._right_node = None
            if widget is not None:
                raise ValueError("SessionNode::left_node -> wrong value '{}' : this must be a Terminal or a SessionNode or None.".format(widget))

    @property
    def count(self):
        if self.left_node is not None and self.right_node is not None:
            return 2
        elif self.left_node is not None:
            return 1
        else:
            return 0

    @property  # getter
    def widget(self):
        if self.left_node is None and self.right_node is None:
            raise Exception("SessionNode::render -> session is empty")

        if self.right_node is None:  # only left child case
            if self.orientation == HORIZONTAL_ORIENTATION:
                box = Gtk.HBox()
            else:
                box = Gtk.VBox()
            box.pack_start(self.left_node.render(), True, True, 0)
            #box_context = box.get_style_context()
            #box_context.add_class('terminal')
            widget = box
        else:  # both children case
            if self.orientation == HORIZONTAL_ORIENTATION:
                paned = Gtk.HPaned()
            else:
                paned = Gtk.VPaned()
            paned.pack1(self.left_node.render(), True, True)
            paned.pack2(self.right_node.render(), True, True)
            widget = paned
        return widget

    @widget.setter
    def widget(self, widget):
        if not isinstance(widget, Gtk.Paned) or not isinstance(widget, Gtk.Box):
            raise Excpetion("SessionNode::widget -> widget must be a Paned or Box.")

        self._widget = widget

    def render(self):
        return self.widget

    def add_terminal(self):
        if self.left_node is None:
            self.left_node = Terminal(session=self.session, session_node=self, background_opacity=0)
            return self.left_node
        elif self.right_node is None:
            self.right_node = Terminal(session=self.session, session_node=self, background_opacity=0)
            return self.right_node
        else:
            if self.session.session_node_on_focus is self:
                if self.session.terminal_on_focus is self.left_node:
                    terminal = self.left_node
                    self.left_node = SessionNode(session=self.session, orientation=self.orientation)
                    self.left_node.left_node = terminal
                    return self.left_node.add_terminal()
                else:
                    terminal = self.right_node
                    self.right_node = SessionNode(session=self.session, orientation=self.orientation)
                    self.right_node.left_node = terminal
                    return self.right_node.add_terminal()

    def destroy_terminal(self, terminal):
        if self.left_node is terminal:
            self.left_node = None
        elif self.right_node is terminal:
            self.right_node = None
        else:
            if self.left_node is not None:
                self.left_node.destroy_terminal(terminal)
            if self.right_node is not None:
                self.right_node.destroy_terminal(terminal)

    def is_empty(self):
        return self.left_node is None and self.right_node is None

    def serialize(self, indentation=2):
        string = '''
{{
    type: SessionNode,
    orientation: {},
    left_node: {},
    right_node: {}
}}'''.format(self.orientation,
             self.left_node.serialize(indentation + 1) if self.left_node is not None else 'None',
             self.right_node.serialize(indentation + 1) if self.right_node is not None else 'None')

        return '\n'.join('    ' * indentation + line for line in string.splitlines())


class Session:
    def __init__(self, orientation, update_screen):
        self.__update_screen = update_screen  # tell Window to update
        self.orientation = orientation
        self.root = None
        self.terminal_on_focus = None

    @property
    def root(self):
        return self._root

    @root.setter
    def root(self, widget):
        if not isinstance(widget, SessionNode) and widget is not None:
            raise ValueError("Session::root -> widget must be a SessionNode or None")

        self._root = widget

    @property  # getter
    def orientation(self):
        return self._orientation

    @orientation.setter  # setter
    def orientation(self, orientation):
        if orientation != HORIZONTAL_ORIENTATION and orientation != VERTICAL_ORIENTATION:
            raise ValueError("Session::orientation -> wrong value '{}' : this is nor horizontal, nor vertical.".format(orientation))

        self._orientation = orientation

    @property
    def terminal_on_focus(self):
        return self._terminal_on_focus

    @terminal_on_focus.setter
    def terminal_on_focus(self, terminal):
        if terminal is not None and not isinstance(terminal, Terminal):
            raise ValueError("Session::terminal_on_focus -> wrong value '{}' : this is nor None, nor Terminal.".format(terminal))

        self._terminal_on_focus = terminal

    @property
    def session_node_on_focus(self):
        if self.terminal_on_focus is None:
            raise ValueError("Session::session_node_on_focus -> terminal_on_focus is None.")

        if self.terminal_on_focus.session_node is None:
            raise ValueError("Session::session_node_on_focus -> session_node_on_focus is None.")

        return self.terminal_on_focus.session_node

    def render(self):
        if self.root is None:
            self.add_terminal()
        return self.root.render()

    def add_terminal(self):
        if self.root is None:
            self.root = SessionNode(session=self, orientation=self.orientation)
            print('SELF.ROOT WAS NONE')
        if self.terminal_on_focus is not None:
            self.terminal_on_focus.session_node.orientation = self.orientation
        terminal = self.root.add_terminal()
        self.terminal_on_focus = terminal
        self.terminal_on_focus.grab_focus()

    def focus_handler(self, terminal):
        self.terminal_on_focus = terminal
        self.terminal_on_focus.grab_focus()

    def destroy_terminal(self, terminal):
        self.root.destroy_terminal(terminal)
        self.__update_screen()

    def serialize(self):
        return '''
{{
    type: Session,
    orientation: {},
    root: {},
}}'''.format(self.orientation,
             self.root.serialize() if self.root is not None else 'None')


