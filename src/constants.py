from gi.repository import Gtk


LOGGING_FILE='/tmp/debug.log'

KEYS_TO_SESSIONS = {
    '1': 0,
    '2': 1,
    '3': 2,
    '4': 3,
    '5': 4,
    '6': 5,
    '7': 6,
    '8': 7,
    '9': 8,
    '0': 9
}

HORIZONTAL_ORIENTATION = Gtk.Orientation.HORIZONTAL  #'horizontal'
VERTICAL_ORIENTATION = Gtk.Orientation.VERTICAL #'vertical'


