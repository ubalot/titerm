# window.py
#
# Copyright 2020 Ubalot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import cairo
from gi.repository import GObject, Gdk, Gtk

from .application import Application
from .constants import KEYS_TO_SESSIONS
from .header_bar import HeaderBar


#@Gtk.Template(resource_path='/org/example/TiTerm/window.ui')
class TitermWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'TitermWindow'

    # https://python-gtk-3-tutorial.readthedocs.io/en/latest/objects.html#create-new-signals
    #__gsignals__ = {
    #    'switch_session': (GObject.SIGNAL_RUN_FIRST, None, (int,))
    #}

    WINDOW_NAME = 'TiTerm'

    DEFAULT_WIDTH = 1200
    DEFAULT_HEIGHT = 600

    background_opacity = 0

    def __init__(self, multiple_sessions=False, background_opacity=0.5, **kwargs):
        self.background_opacity = background_opacity

        super().__init__(**kwargs)

        self.set_default_size(self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT)

        self.app = Application(update_screen=self.update_screen)

        self.init_shortcuts()

        # Binding signals
        print(dir(self.props))
        #self.connect('focus_change', self.focus_change)
        self.connect('destroy', lambda _: self.destroy())
        #self.connect('terminal_destroyed_signal', self.update)

        # Apply window background settings.
        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)
        self.set_app_paintable(True)

        # Header Bar
        self.header_bar = HeaderBar(title=self.WINDOW_NAME,
                                    multiple_sessions=multiple_sessions,
                                    switch_session_func=self.do_switch_to_session)
        self.set_titlebar(self.header_bar)
        self.header_bar.activate_session_button(self.app.active_session_idx)

        # Content
        self.content = Gtk.Stack()
        self.content.set_transition_duration(500)

        # Content::Session
        name = self.active_session_name
        widget = self.app.active_session.render()
        self.content.add_named(widget, name)
        self.content.set_visible_child_full(name, Gtk.StackTransitionType.NONE)

        self.add(self.content)
        #print(self.app.active_session.serialize())

        self.show_all()  # Show window

        #self.width, self.height = self.get_size()

        #print('window',dir(self))

    def update_screen(self):
        name = self.active_session_name
        self.__detach_session_widget(name)
        try:
            self.__attach_session_widget(name)
        except Exception:
            if not self.app.active_session.root.is_empty():
                raise Exception("TitermWindow::update_screen -> self.__attach_session_widget raised an exception but self.app.active_session.root is not empty")

            no_active_session_left = True
            for session in KEYS_TO_SESSIONS.values():
                if (self.app.sessions[session].root is not None and
                    not self.app.sessions[session].root.is_empty()):
                    self.do_switch_to_session(None, session)
                    no_active_session_left = False
                    break

            if no_active_session_left:
                self.destroy()  # destroy application and exit
        else:
            self.show_all()

    @property
    def active_session_name(self):
        return str(self.app.active_session_idx)

    def __detach_session_widget(self, name):
        widget = self.content.get_child_by_name(name)
        #print('widget', widget, self.app.active_session.serialize())

        if widget is None:  # this check is necessary when closing application from headerbar close-button
            return

        for child in widget.get_children():
            widget.remove(child)
        self.content.remove(widget)
        widget.destroy()

    def __attach_session_widget(self, name):
        widget = self.app.active_session.render()
        if self.content.get_child_by_name(name):
            raise Exception("Child {} already exists".format(name))
        self.content.add_named(widget, name)
        widget.show()
        print('----------------------')
        print('widget', widget)

    def add_terminal(self):
        name = self.active_session_name

        self.__detach_session_widget(name)

        print("self.content.get_children()",self.content.get_children(), len(self.content.get_children()))
        self.app.active_session.add_terminal()

        self.__attach_session_widget(name)
        self.content.set_visible_child_full(name, Gtk.StackTransitionType.NONE)
        self.show_all()
        print(self.app.active_session.serialize())


    def focus_change(self):
        print('FOCUS CHANGE event captured!!!!')

    def init_shortcuts(self):
        accelerators = Gtk.AccelGroup()

        for i in range(len(KEYS_TO_SESSIONS)):
            #key, mod = Gtk.accelerator_parse(f'<Alt>{i}')
            key, mod = Gtk.accelerator_parse(f'<Ctrl>{i}')
            accelerators.connect(key, mod, Gtk.AccelFlags.VISIBLE, self.on_accel_pressed)

        #shortcuts = ['<Alt>h', '<Alt>v']  # 104, 118
        shortcuts = ['<Ctrl>h', '<Ctrl>v']  # 104, 118
        for shortcut in shortcuts:
            key, mod = Gtk.accelerator_parse(shortcut)
            accelerators.connect(key, mod, Gtk.AccelFlags.VISIBLE, self.on_accel_pressed)

        #key, mod = Gtk.accelerator_parse('<Alt>Return')
        key, mod = Gtk.accelerator_parse('<Ctrl>Return')
        accelerators.connect(key, mod, Gtk.AccelFlags.VISIBLE, self.on_accel_pressed)

        self.add_accel_group(accelerators)

    def on_accel_pressed(self, accel_group, window, key, mod_mask):
        keyval = Gdk.keyval_name(key)
        #print('eccomi, click -> ', accel_group, window, key, keyval, type(keyval), mod_mask)
        #if mod_mask == Gdk.ModifierType.MOD1_MASK:  # Alt
        if mod_mask == Gdk.ModifierType.CONTROL_MASK:  # Ctrl
            if keyval == 'Return':
                self.add_terminal()

            elif keyval == 'h':
                self.app.set_horizontal_mode()
            elif keyval == 'v':
                self.app.set_vertical_mode()

            elif keyval >= '0' and keyval <= '9':
                session = KEYS_TO_SESSIONS[keyval]
                self.do_switch_to_session(None, session)

    #######################
    # Actions
    #######################
    def do_switch_to_session(self, widget, session_idx):
        if session_idx == self.app.active_session_idx:
            return  # there is no change to do

        if widget is None:  # shortcut handled
            self.header_bar.activate_session_button(session_idx)
        else:  # session button clicked
            if self.app.active_session_idx < session_idx:
                transition = Gtk.StackTransitionType.OVER_LEFT
            else:
                transition = Gtk.StackTransitionType.OVER_RIGHT
            self.app.switch_to_session(session_idx)
            name = self.active_session_name
            widget = self.content.get_child_by_name(name)
            if widget is None:
                widget = self.app.active_session.render()
                self.content.add_named(widget, name)
            widget.show()
            self.content.set_visible_child_full(name, transition)
            self.show_all()
            self.app.active_session.terminal_on_focus.grab_focus()
            #print(self.app.active_session.terminal_on_focus)
            #print(self.app.active_session.serialize())


