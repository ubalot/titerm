from collections import defaultdict

from gi.repository import GObject, Gdk, Gtk

from .constants import HORIZONTAL_ORIENTATION, VERTICAL_ORIENTATION
from .constants import KEYS_TO_SESSIONS
from .session import Session


class Application:
    def __init__(self, update_screen):
        self.active_mode = HORIZONTAL_ORIENTATION
        self.sessions = [Session(self.active_mode, update_screen=update_screen) for _ in range(len(KEYS_TO_SESSIONS))]
        #print("self.sessions", self.sessions)
        #self.terminals = defaultdict(list)
        self.active_session_idx = 0
        self.switch_to_session(self.active_session_idx)

    # --------------- MODE ------------------------------
    @property
    def active_mode(self):
        return self.__active_mode

    @active_mode.setter
    def active_mode(self, orientation):
        if orientation not in [HORIZONTAL_ORIENTATION, VERTICAL_ORIENTATION]:
            raise ValueError("Application::active_mode -> orientation must be HORIZONTAL_ORIENTATION or VERTICAL_ORIENTATION")

        self.__active_mode = orientation

    def set_vertical_mode(self):
        self.active_mode = VERTICAL_ORIENTATION
        print('mode: ', self.active_mode)
        self.active_session.orientation = self.active_mode

    def set_horizontal_mode(self):
        self.active_mode = HORIZONTAL_ORIENTATION
        print('mode: ', self.active_mode)
        self.active_session.orientation = self.active_mode

    #def toggle_mode(self):
    #    if self.active_mode == HORIZONTAL_ORIENTATION:
    #        self.active_mode = VERTICAL_ORIENTATION
    #    else:
    #        self.active_mode = HORIZONTAL_ORIENTATION

    # --------------- SESSION ------------------------------
    @property  # getter
    def active_session_idx(self):
        return self.__active_session_idx

    @active_session_idx.setter  # setter
    def active_session_idx(self, session_idx):
        if not isinstance(session_idx, int):
            raise ValueError("Application::active_session_idx -> session_idx must be an integer.")

        if session_idx not in range(len(self.sessions)):
            raise ValueError("Application::active_session_idx -> session_idx not in range {}.".format(len(self.sessions)))

        self.__active_session_idx = session_idx

    @property  # getter
    def active_session(self):
        return self.sessions[self.active_session_idx]

    @active_session.setter  # setter
    def active_session(self, session_widget):
        if not isinstance(session_widget, Gtk.Widget):
            raise ValueError('Application::active_session -> active_session must be a Widget.')

        self.sessions[self.active_session_idx] = session_widget

    def switch_to_session(self, session_idx):
        if not isinstance(session_idx, int):
            raise ValueError("Application::switch_to_session -> session_idx must be an integer.")

        print('Application::switch_to_session -> from {} to {}'.format(self.active_session_idx, session_idx))
        self.active_session_idx = session_idx  # change active session (only logic)
        self.active_session.orientation = self.active_mode  # update orientation


