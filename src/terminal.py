import gi
gi.require_version('Vte', '2.91')

from gi.repository import Gdk, Gio, Gtk, Vte, GObject
from gi.repository import GLib
import gobject

import os


linux_theme = {
    "name": "Linux",
    "palette": [
        "#000000",
        "#AA0000",
        "#00AA00",
        "#AA5400",
        "#0000AA",
        "#AA00AA",
        "#00AAAA",
        "#AAAAAA",
        "#545454",
        "#FF5454",
        "#54FF54",
        "#FFFF54",
        "#5454FF",
        "#FF54FF",
        "#54FFFF",
        "#FFFFFF"
    ]
}

material_theme = {
    "name": "Material",
    "comment": "Material Oceanic Scheme",
    "foreground-color": "#A1B0B8",
    "background-color": "#263238",
    "palette": [
      "#252525",
      "#FF5252",
      "#C3D82C",
      "#FFC135",
      "#42A5F5",
      "#D81B60",
      "#00ACC1",
      "#F5F5F5",
      "#708284",
      "#FF5252",
      "#C3D82C",
      "#FFC135",
      "#42A5F5",
      "#D81B60",
      "#00ACC1",
      "#F5F5F5"
    ]
}

monokai_theme = {
    "name": "Monokai Dark",
    "foreground-color": "#F8F8F2",
    "background-color": "#272822",
    "palette": [
        "#272822",
        "#f92672",
        "#a6e22e",
        "#f4bf75",
        "#66d9ef",
        "#ae81ff",
        "#a1efe4",
        "#f8f8f2",
        "#75715e",
        "#f92672",
        "#a6e22e",
        "#f4bf75",
        "#66d9ef",
        "#ae81ff",
        "#a1efe4",
        "#f9f8f5"
    ]
}

solarized_dark_theme = {
    "name": "Solarized Dark",
    "foreground-color": "#839496",
    "background-color": "#002B36",
    "palette": [
        "#073642",
        "#DC322F",
        "#859900",
        "#B58900",
        "#268BD2",
        "#D33682",
        "#2AA198",
        "#EEE8D5",
        "#002B36",
        "#CB4B16",
        "#586E75",
        "#657B83",
        "#839496",
        "#6C71C4",
        "#93A1A1",
        "#FDF6E3"
    ]
}

solarized_light_theme = {
    "name": "Solarized Light",
    "foreground-color": "#657B83",
    "background-color": "#FDF6E3",
    "palette": [
        "#073642",
        "#DC322F",
        "#859900",
        "#B58900",
        "#268BD2",
        "#D33682",
        "#2AA198",
        "#EEE8D5",
        "#002B36",
        "#CB4B16",
        "#586E75",
        "#657B83",
        "#839496",
        "#6C71C4",
        "#93A1A1",
        "#FDF6E3"
    ]
}

yaru_theme = {
    "name": "Yaru",
    "foreground-color": "#ffffff",
    "background-color": "#300a24",
    "palette": [
        "#2E3436",
        "#CC0000",
        "#4E9A06",
        "#C4A000",
        "#3465A4",
        "#75507B",
        "#06989A",
        "#D3D7CF",
        "#555753",
        "#EF2929",
        "#8AE234",
        "#FCE94F",
        "#729FCF",
        "#AD7FA8",
        "#34E2E2",
        "#EEEEEC"
    ]
}


class TerminalTheme:
    def __init__(self, theme, background_alpha=1.0):
        if 'foreground-color' in theme:
            foreground_color = Gdk.RGBA()
            foreground_color.parse(theme['foreground-color'])
        else:
            foreground_color = None

        if 'background-color' in theme:
            background_color = Gdk.RGBA()
            background_color.parse(theme['background-color'])
            background_color.alpha = background_alpha  # 0.75 is the minimum for readability
        else:
            background_color = None

        palette = []
        for color in theme['palette']:
            c = Gdk.RGBA()
            c.parse(color)
            palette.append(c)

        self.foreground_color = foreground_color
        self.background_color = background_color
        self.palette = palette


class Terminal(Vte.Terminal):
    #__gsignals__ = {
    #    'terminal_destroyed_signal': (GObject.SIGNAL_RUN_LAST, GObject.TYPE_NONE, ()),
    #}

    def __init__(self, session=None, session_node=None, background_opacity=0, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.session = session  # terminal belongs to this Session
        self.session_node = session_node  # terminal is child of this SessionNode

        self.theme = TerminalTheme(yaru_theme, 0.75)
        self.set_colors(self.theme.foreground_color, self.theme.background_color, self.theme.palette)

        self.set_can_focus(True)
        self.get_focus_on_click()

        self.connect("child-exited", self.quit)
        self.connect("focus-in-event", self.focus_in_handler)

        # https://lazka.github.io/pgi-docs/Vte-2.91/classes/Terminal.html#Vte.Terminal.spawn_sync
        self.spawn_sync(
            Vte.PtyFlags.DEFAULT,
            os.environ['HOME'],
            ["/bin/bash"],
            [],
            GLib.SpawnFlags.DO_NOT_REAP_CHILD,
            None,
            None,
            )

    @property
    def widget(self):
        return self

    def render(self):
        return self.widget

    def quit(self, widget, event):
        print('terminal destroy', self, widget)
        print('self.session', self.session, self.session.root, self == widget)
        self.session.destroy_terminal(widget)

    def focus_in_handler(self, widget, event):
        print("FOCUS", widget, event)
        self.session.focus_handler(self)

    def serialize(self, indentation=2):
        string = '''
{
    type: Terminal
}'''
        return '\n'.join('    ' * indentation + line for line in string.splitlines())


#print('TErminal is gobject?', isinstance(Terminal, GObject))
#GObject.type_register(Terminal)
#GObject.signal_new('terminal_destroyed_signal',
#                   Terminal,
#                   GObject.SIGNAL_RUN_FIRST,
#                   GObject.TYPE_NONE, ())
